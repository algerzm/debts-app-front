import { createTheme } from '@mui/material'

const MainTheme = createTheme({
  components: {
    MuiDrawer: {
      styleOverrides: {
        paper: {
          width: '15vw',
        },
      },
    },
  },
  palette: {
    background: {
      default: '#1e1e2d',
    },
    primary: {
      main: '#1e1e2d',
      light: '#464556',
      dark: '#000002',
    },
    secondary: {
      main: '#ffeb3b',
      light: '#ffff72',
      dark: '#c8b900',
    },
    terciary: {
      main: '#29b6f6',
      light: '#73e8ff',
      dark: '#0086c3',
    },
  },
})

export default MainTheme
