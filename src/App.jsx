import { Route, Routes } from 'react-router-dom'
import { ThemeProvider as MuiThemeProvider, CssBaseline } from '@mui/material'
import { ThemeProvider } from 'styled-components'
import MainTheme from './theme/MainTheme'

//Views
import LoginView from './views/LoginView'
import MainView from './views/MainView'
import NotFoundView from './views/NotFoundView'
import Tes from './views/tes'
import Dashboard from './views/mainPanelViews/Dashboard'
import NewAccount from './views/NewAccount'

//Redux
import { Provider } from 'react-redux'
import store from './store/store'

function App() {
  return (
    <MuiThemeProvider theme={MainTheme}>
      <CssBaseline />
      <ThemeProvider theme={MainTheme}>
        <Provider store={store}>
          <Routes>
            <Route path="/" element={<LoginView />}></Route>
            <Route path="/new-account" element={<NewAccount />}></Route>
            <Route path="/tes" element={<Tes />}></Route>
            <Route path="dashboard" element={<MainView />}>
              <Route index path="" element={<Dashboard />}></Route>
            </Route>
            <Route path="*" element={<NotFoundView></NotFoundView>}></Route>
          </Routes>
        </Provider>
      </ThemeProvider>
    </MuiThemeProvider>
  )
}

export default App
