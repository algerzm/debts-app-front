import axiosClient from '../axiosClient'
//email: 'test@test.com',
//password: 'T3st123!'
const authUser = async (email, password) => {
  let res = axiosClient.post('/auth/login', {
    user: {
      email: email,
      password: password,
    },
  })

  return res
}

export { authUser }
