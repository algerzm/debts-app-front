import axios from 'axios'

const dev = 'http://localhost:4000/'
const prod = 'https://debtsapi.nitramgb.com/'
const pcnavi = 'http://192.168.3.27:4000/'

const axiosClient = axios.create({
  baseURL: prod,
})

axiosClient.interceptors.request.use((config) => {
  if (config.method === 'get') {
    console.log('Doing get')
  }
  return config
})

axiosClient.interceptors.response.use(
  (res) => {
    console.log('Response del succes')
    return res
  },
  (error) => {
    return Promise.reject(error.response)
  }
)

export default axiosClient
