import axiosClient from '../axiosClient'

const createUser = async (email, name, lastname, gender, password) => {
  let res = axiosClient.post('/users/sign_up', {
    user: {
      name: name,
      lastname: lastname,
      email: email,
      gender: gender,
      creationDate: '01/01/21',
      password: password,
    },
  })

  return res
}

export { createUser }
