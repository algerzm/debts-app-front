import React from 'react'

import {Grid} from '@mui/material'

export default function LoginHeader(props) {
    return <Grid sx={{display:'flex', justifyContent:'center'}}  item xs={12}>{props.children}</Grid>
}
