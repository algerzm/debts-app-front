import React from 'react'
import styled from 'styled-components'

const LoginContainerStyled = styled('div')`
  ${({ theme }) => `
    display: flex;
    justify-content: center;
    align-items: center;
    height: 100vh;
    width: 100vw;
  `}
`

export default function LoginContainer(props) {
  return <LoginContainerStyled>{props.children}</LoginContainerStyled>
}
