import React from 'react'
import styled from 'styled-components'
import { Box } from '@mui/material'

const LoginBoxComponent = styled(Box)`
  ${({ theme }) => `
    display: flex;
    height: auto;
    width: 90vw;
    minHeight: 500px;
    max-width: 500px;
    background-color: white;
    padding: 3rem;
  `}
`
const NewAccountBoxComponent = styled(Box)`
  ${({ theme }) => `
    display: flex;
    height: auto;
    width: 60vw;
    minHeight: 500px;
    max-width: 900px;
    min-width: 400px;
    background-color: white;
    padding: 3rem;
  `}
`

export default function LoginBox(props) {
  return props.multiple ? (
    <NewAccountBoxComponent>{props.children}</NewAccountBoxComponent>
  ) : (
    <LoginBoxComponent>{props.children}</LoginBoxComponent>
  )
}
