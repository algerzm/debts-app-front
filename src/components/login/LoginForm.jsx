import React, { useState, useEffect } from 'react'
import { TextField, Grid } from '@mui/material'
import LoadingButton from '@mui/lab/LoadingButton'
import LoginBox from './styled/LoginBox'
import LoginHeader from './styled/LoginHeader'
import LoginButtonContainer from './styled/LoginButtonContainer'
import { toast, ToastContainer } from 'react-toastify'
import { useNavigate } from 'react-router-dom'

import logo from '../../assets/Logo.png'
import { authUser } from '../../api/auth/authController'
import useLoginForm from '../../hooks/useLoginForm'

export default function LoginForm() {
  const { formInputs, validationError, handleInputChange, validateInput } = useLoginForm()
  const [loading, setLoading] = useState(false)
  const [error, setError] = useState(false)

  const { email, password } = formInputs
  const goToDashboard = () => navigate('/dashboard')
  let navigate = useNavigate()

  useEffect(() => {
    let token = localStorage.getItem('token') || undefined
    token ? goToDashboard() : null
  }, [])

  const handleSubmit = async (e) => {
    e.preventDefault()
    setLoading(true)

    await authUser(email, password)
      .then((res) => {
        toast.success(res.data.access_token)
        setLoading(false)
        localStorage.setItem('user', JSON.stringify(res.data.user))
        localStorage.setItem('token', res.data.access_token)
        goToDashboard()
      })
      .catch((err) => {
        let errorMessage = err.status === 404 ? 'User or password are not valid.' : 'Something went wrong!'
        toast.error(errorMessage)
        setLoading(false)
      })
  }

  return (
    <LoginBox>
      <ToastContainer />
      <Grid container component="form" spacing={5}>
        <LoginHeader>
          <img src={logo} style={{ width: '25vh' }} />
        </LoginHeader>
        <Grid item xs={12}>
          <TextField
            error={validationError.email}
            helperText={validationError.helperEmailText}
            disabled={loading}
            value={email}
            onBlur={validateInput}
            name="email"
            onChange={handleInputChange}
            label="E-Mail"
            fullWidth
            variant="outlined"
          ></TextField>
        </Grid>
        <Grid item xs={12}>
          <TextField
            name="password"
            disabled={loading}
            error={validationError.password}
            helperText={validationError.helperPasswordText}
            onChange={handleInputChange}
            onBlur={validateInput}
            label="Password"
            value={password}
            autoComplete="on"
            type="password"
            fullWidth
            variant="outlined"
          />
        </Grid>
        <LoginButtonContainer>
          <LoadingButton
            disabled={validationError.buttonDisabled}
            loading={loading}
            onClick={handleSubmit}
            variant="contained"
            sx={{ width: '50%' }}
            color="primary"
            type="submit"
          >
            Sign In
          </LoadingButton>
        </LoginButtonContainer>
      </Grid>
    </LoginBox>
  )
}
