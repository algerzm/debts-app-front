import { Typography } from '@mui/material'
import HeaderContainer from './styled/HeaderContainer'
import SearchTextField from '../utils/SearchTextField'

export default function DashboardHeader({ title, description, placeholderText }) {
  return (
    <HeaderContainer>
      <div>
        <Typography variant="h4">{title}</Typography>
        <Typography variant="subtitle1">{description}</Typography>
      </div>
      <div>
        <SearchTextField />
      </div>
    </HeaderContainer>
  )
}
