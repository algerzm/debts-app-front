import { Typography } from '@mui/material'
import { AttachMoney } from '@mui/icons-material'
import ResumePaper from './styled/ResumePaper'

const styles = {
  titleContainer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  balanceContainer: {
    display: 'flex',
    flexDirection: 'row',
  },
  dolarIcon: {
    marginTop: '0.55rem',
    marginRight: '0.2rem',
  },
  amountText: {
    marginTop: '0.5rem',
  },
}

export default function ResumeCard({ colorIcon, title, amount, TitleIcon }) {
  return (
    <ResumePaper>
      <div style={styles.titleContainer}>
        <Typography variant="body1">{title}</Typography>
        <TitleIcon sx={{ color: colorIcon }}></TitleIcon>
      </div>
      <div style={styles.balanceContainer}>
        <AttachMoney fontSize="large" sx={styles.dolarIcon}></AttachMoney>
        <Typography variant="h4" sx={styles.amountText}>
          {amount}
        </Typography>
      </div>
    </ResumePaper>
  )
}
