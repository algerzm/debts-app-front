import { Avatar, Tooltip, Zoom } from '@mui/material'

export default function FriendItemAvatar({ name }) {
  return (
    <Tooltip title={name} placement="bottom" TransitionComponent={Zoom}>
      <Avatar sx={styles.avatar}>A</Avatar>
    </Tooltip>
  )
}

const styles = {
  avatar: {
    width: '70px',
    height: '70px',
    marginRight: '1.5rem',
    fontSize: '2rem',
  },
}
