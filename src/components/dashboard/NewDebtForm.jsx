import { useState } from 'react'
import {
  Typography,
  Paper,
  Select,
  FormControl,
  Button,
  MenuItem,
  TextField,
  InputLabel,
  OutlinedInput,
  InputAdornment,
} from '@mui/material'

export default function NewDebtForm({ notify }) {
  const [debtType, setDebtType] = useState(1)

  const handleDebtTypeChange = (e) => {
    setDebtType(e.target.value)
  }
  return (
    <>
      <Typography color="white" variant="h5" sx={styles.formTitle}>
        Create a new Debt
      </Typography>
      <Paper sx={styles.debtPaper}>
        <div style={styles.formHeaderContainer}>
          <Typography variant="h5">New Debt</Typography>
          <Select onChange={handleDebtTypeChange} value={debtType}>
            <MenuItem value={1}>Owed by Me</MenuItem>
            <MenuItem value={2}>Owed to Me</MenuItem>
          </Select>
        </div>
        <TextField fullWidth label="Title/Description" sx={styles.titleTextField}></TextField>
        <FormControl fullWidth>
          <InputLabel htmlFor="totalAmountInput">Total Amount</InputLabel>
          <OutlinedInput
            id="totalAmountInput"
            startAdornment={<InputAdornment position="start">$</InputAdornment>}
            fullWidth
            label="Total Amount"
            placeholder="9.99"
          ></OutlinedInput>
        </FormControl>
        <div style={styles.buttonContainer}>
          <Button disableElevation variant="contained" sx={styles.createButton} color="secondary" onClick={() => notify('Debt Created!')}>
            Create
          </Button>
        </div>
      </Paper>
    </>
  )
}

const styles = {
  formTitle: {
    marginBottom: '1rem',
  },
  debtPaper: {
    backgroundColor: 'white',
    width: '100%',
    height: '81%',
    borderRadius: 0,
    padding: 3,
  },
  formHeaderContainer: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: '3rem',
  },
  titleTextField: {
    marginBottom: '5vh',
  },
  buttonContainer: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: '3rem',
    marginBottom: '5rem',
  },
  createButton: {
    width: '70%',
    fontWeight: 'bold',
  },
}
