import { Avatar, Typography } from '@mui/material'
import { Scrollbars } from 'react-custom-scrollbars-2'
import FriendItemAvatar from './FriendItemAvatar'

export default function OnlineFriendsSection() {
  return (
    <>
      <Typography variant="h5" color="white" sx={styles.title}>
        Online Friends
      </Typography>
      <Scrollbars style={styles.scrollbar}>
        <div style={styles.avatarsContainer}>
          <FriendItemAvatar name="Jose">A</FriendItemAvatar>
          <FriendItemAvatar name="Jose"></FriendItemAvatar>
          <FriendItemAvatar name="Jose"></FriendItemAvatar>
          <FriendItemAvatar name="Jose"></FriendItemAvatar>
          <FriendItemAvatar name="Jose"></FriendItemAvatar>
          <FriendItemAvatar name="Jose"></FriendItemAvatar>
          <FriendItemAvatar name="Jose"></FriendItemAvatar>
          <FriendItemAvatar name="Jose"></FriendItemAvatar>
          <FriendItemAvatar name="Jose"></FriendItemAvatar>
          <FriendItemAvatar name="Jose">A</FriendItemAvatar>
          <FriendItemAvatar name="Jose"></FriendItemAvatar>
          <FriendItemAvatar name="Jose"></FriendItemAvatar>
          <FriendItemAvatar name="Jose"></FriendItemAvatar>
          <FriendItemAvatar name="Jose"></FriendItemAvatar>
          <FriendItemAvatar name="Jose"></FriendItemAvatar>
          <FriendItemAvatar name="Jose"></FriendItemAvatar>
          <FriendItemAvatar name="Jose"></FriendItemAvatar>
          <FriendItemAvatar name="Jose"></FriendItemAvatar>
        </div>
      </Scrollbars>
    </>
  )
}

const styles = {
  title: {
    marginBottom: 2,
  },
  scrollbar: {
    height: '6rem',
  },
  avatarsContainer: {
    display: 'flex',
    paddingBottom: '1rem',
  },
}
