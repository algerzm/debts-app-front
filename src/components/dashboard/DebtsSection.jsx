import React from 'react'
import { Typography, Button } from '@mui/material'

import DebtItem from './DebtItem'

export default function DebtsSection({ debts, title }) {
  const getDebtItems = () =>
    debts.map((debt) => (
      <DebtItem key={debt.title} title={debt.title} totalAmount={debt.totalAmount} amountLeft={debt.leftAmount}></DebtItem>
    ))

  return (
    <div>
      <Typography color="white" variant="h5" sx={styles.title}>
        {title}
      </Typography>
      {getDebtItems()}
      <div style={styles.buttonContainer}>
        <Button variant="contained" color="secondary" sx={styles.viewMoreButton}>
          View More
        </Button>
      </div>
    </div>
  )
}

const styles = {
  buttonContainer: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    marginTop: '2rem',
  },
  viewMoreButton: {
    fontWeight: 'bold',
    borderRadius: 0,
    width: '30%',
    minWidth: '200px',
    marginBottom: '2rem',
  },
  title: {
    marginBottom: '1rem',
  },
}
