import { Typography } from '@mui/material'
export default function NoDebtsToShow() {
  return (
    <div style={styles.noDebtsContainer}>
      <Typography variant="h6" color="white">
        You have no debts to show :D
      </Typography>
    </div>
  )
}

const styles = {
  noDebtsContainer: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: '50%',
  },
}
