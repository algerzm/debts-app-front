import { Avatar, Paper, Typography, Button } from '@mui/material'

export default function DebtItem({ title, amountLeft, avatar, totalAmount }) {
  return (
    <Paper
      sx={{
        backgroundColor: 'white',
        width: '100%',
        height: '80px',
        borderRadius: 0,
        marginBottom: '1rem',
        display: 'flex',
        alignItems: 'center',
      }}
    >
      <div style={{ float: 'left', height: '100%', width: '5px', backgroundColor: '#FFF833' }}></div>
      <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', width: '10%', height: '100%', minWidth: '80px' }}>
        <Avatar>{avatar || null}</Avatar>
      </div>
      <div style={{ height: '80%', width: '1px', backgroundColor: '#DCDCDC' }}></div>
      <div
        style={{
          paddingLeft: '1.5rem',
          display: 'flex',
          justifyContent: 'space-between',
          width: '100%',
          paddingRight: '1.5rem',
          height: '100%',
        }}
      >
        <div style={{ display: 'flex', flexDirection: 'column', justifyContent: 'center' }}>
          <Typography variant="h6">{title}</Typography>
          <Typography variant="body2">
            Total: {totalAmount} peso / Restante: {amountLeft} peso
          </Typography>
        </div>
        <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
          <Button disableElevation sx={{ borderRadius: 0, marginRight: '0.5rem' }} variant="contained">
            Modificar
          </Button>
          <Button disableElevation sx={{ borderRadius: 0 }} variant="contained">
            Pagar
          </Button>
        </div>
      </div>
    </Paper>
  )
}
