import { Paper } from '@mui/material'
import { styled } from '@mui/material/styles'

const ResumeCardStyled = styled(Paper)(({ theme }) => ({
  width: '100%',
  height: 'auto',
  backgroundColor: 'white',
  padding: '1rem',
  borderRadius: 0,
}))

export default function ResumePaper({ children }) {
  return <ResumeCardStyled>{children}</ResumeCardStyled>
}
