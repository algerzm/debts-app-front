import React from 'react'
import { styled } from '@mui/material/styles'

const DashboardHeaderStyled = styled('div')(({ theme }) => ({
  width: '100%',
  color: 'white',
  marginTop: '20px',
  display: 'flex',
  justifyContent: 'space-between',
  flexWrap: 'wrap',
  marginBottom: '30px',
}))

export default function HeaderContainer({ children }) {
  return <DashboardHeaderStyled>{children}</DashboardHeaderStyled>
}
