import React from 'react'
import { styled } from '@mui/material/styles'

export default function DrawerHeader({ children }) {
  const DrawerHeaderComponent = styled('div')(({ theme }) => ({
    display: 'flex',
    alignItems: 'center',
    padding: theme.spacing(0, 1),
    justifyContent: 'flex-start',
  }))

  return <DrawerHeaderComponent>{children}</DrawerHeaderComponent>
}
