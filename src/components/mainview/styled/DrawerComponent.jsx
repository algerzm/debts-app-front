import React from 'react'
import { Drawer } from '@mui/material'
import { styled } from '@mui/material/styles'

const container = window !== undefined ? () => window().document.body : undefined

const MainDrawer = styled(Drawer)(({ theme, open }) => ({
  transition: theme.transitions.create('width', {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  ...(open && {
    '& .MuiDrawer-paper': {
      backgroundColor: 'white',
      width: '200px',
      transition: theme.transitions.create('width', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
    },
  }),
  ...(!open && {
    '& .MuiDrawer-paper': {
      backgroundColor: 'white',
      width: '60px',
      overflowX: 'hidden',
      transition: theme.transitions.create('width', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
    },
  }),
}))

export default function DrawerComponent({ open, children }) {
  return (
    <MainDrawer open={open} variant="permanent">
      {children}
    </MainDrawer>
  )
}
