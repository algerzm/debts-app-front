import React from 'react'
import { ListItem, ListItemIcon, ListItemText } from '@mui/material'

import MailIcon from '@mui/icons-material/Mail'
import AllInboxIcon from '@mui/icons-material/AllInbox'

export default function DrawerItemTooltip({ item, index }) {
  return (
    <ListItem button>
      <ListItemIcon>{index % 2 === 0 ? <AllInboxIcon></AllInboxIcon> : <MailIcon></MailIcon>}</ListItemIcon>
      <ListItemText
        primaryTypographyProps={{
          style: {
            overflow: 'hidden',
            textOverflow: 'ellipsis',
            whiteSpace: 'nowrap',
          },
        }}
        primary={item.text}
      ></ListItemText>
    </ListItem>
  )
}
