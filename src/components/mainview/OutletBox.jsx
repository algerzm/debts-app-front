import { Box } from '@mui/material'
import { styled } from '@mui/material/styles'

const OutletBoxComponent = styled(Box)(({ theme, open, movil }) => ({
  display: 'flex',
  flexGrow: 1,
  paddingTop: '0.1rem',
  paddingLeft: '90px',
  paddingRight: '30px',
  transition: theme.transitions.create('padding-left', {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  flexWrap: 'wrap',
  ...(!open && { paddingLeft: '90px' }),
  ...(open && {
    paddingLeft: '240px',
    ...(movil && {
      paddingLeft: '90px',
    }),
  }),
}))

export default function OutletBox({ children, open, movil }) {
  return (
    <OutletBoxComponent open={open} movil={movil}>
      {children}
    </OutletBoxComponent>
  )
}
