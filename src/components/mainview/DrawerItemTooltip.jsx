import React from 'react'
import { ListItem, ListItemIcon, ListItemText, Tooltip, Zoom } from '@mui/material'
import MailIcon from '@mui/icons-material/Mail'
import AllInboxIcon from '@mui/icons-material/AllInbox'

export default function DrawerItemTooltip({ item, index }) {
  return (
    <Tooltip title={item.text} placement="right" TransitionComponent={Zoom}>
      <ListItem button>
        <ListItemIcon>{index % 2 === 0 ? <AllInboxIcon></AllInboxIcon> : <MailIcon></MailIcon>}</ListItemIcon>
        <ListItemText
          primaryTypographyProps={{
            style: {
              overflow: 'hidden',
              textOverflow: 'ellipsis',
              whiteSpace: 'nowrap',
            },
          }}
          primary={item.text}
        ></ListItemText>
      </ListItem>
    </Tooltip>
  )
}
