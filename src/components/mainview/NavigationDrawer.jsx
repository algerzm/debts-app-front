import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { IconButton, Divider, List } from '@mui/material'
import ChevronRightIcon from '@mui/icons-material/ChevronRight'
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft'

import DrawerComponent from '../mainview/styled/DrawerComponent'
import LoadingSpinner from '../utils/LoadingSpinner'
import DrawerItem from './DrawerItem'
import DrawerItemTooltip from './DrawerItemTooltip'
import DrawerHeader from './styled/DrawerHeader'

//Redux actions
import { toggleDrawerAction, changeItemsAction } from '../../store/actions/drawerActions'

export default function NavigationDrawer() {
  //Use useDispatch to create function
  const dispatch = useDispatch()

  //Get State Variables
  const { drawerItems, loading, error, open } = useSelector((state) => state.drawer)

  //Call action of drawerActions
  const toggleDrawer = () => dispatch(toggleDrawerAction())
  const getDrawerItems = () => dispatch(changeItemsAction())

  //Toggle drawer button
  const handleDrawerClick = () => toggleDrawer()

  const getDrawerItemsTooltip = (items) => items.map((item, index) => <DrawerItemTooltip item={item} index={index} key={item.id} />)

  const getDrawerItemsWithoutTooltip = (items) => items.map((item, index) => <DrawerItem item={item} index={index} key={item.id} />)

  useEffect(() => {
    getDrawerItems()
  }, [])

  return (
    <DrawerComponent open={open}>
      {loading ? (
        <LoadingSpinner />
      ) : (
        <div>
          <DrawerHeader>
            <IconButton color="primary" aria-label="open drawer" onClick={handleDrawerClick} edge="end">
              {open ? <ChevronLeftIcon /> : <ChevronRightIcon />}
            </IconButton>
          </DrawerHeader>
          <Divider></Divider>
          <List>
            {drawerItems.length > 0 ? (open ? getDrawerItemsWithoutTooltip(drawerItems) : getDrawerItemsTooltip(drawerItems)) : null}
          </List>
          <DrawerItem item={{ text: 'Sign Out' }} index={999} key={999} />
        </div>
      )}
    </DrawerComponent>
  )
}
