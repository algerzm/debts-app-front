import { useState } from 'react'
import { TextField, Grid } from '@mui/material'
import LoadingButton from '@mui/lab/LoadingButton'
import LoginBox from '../login/styled/LoginBox'
import LoginHeader from '../login/styled/LoginHeader'
import LoginButtonContainer from '../login/styled/LoginButtonContainer'
import { toast, ToastContainer } from 'react-toastify'
import { useNavigate } from 'react-router-dom'

import logo from '../../assets/Logo.png'
import { createUser } from '../../api/users/usersController'

import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'

import { signUpValidationSchema } from '../../utils/validationSchema'

export default function LoginForm() {
  const formOptions = {
    resolver: yupResolver(signUpValidationSchema),
    mode: 'onBlur',
  }

  const {
    register,
    handleSubmit,
    formState: { errors, isValid },
  } = useForm(formOptions)

  const [loading, setLoading] = useState(false)

  const goToDashboard = () => navigate('/dashboard')
  let navigate = useNavigate()

  const onSubmit = async (data) => {
    setLoading(true)
    const { email, name, lastName, gender, password } = data
    await createUser(email, name, lastName, gender, password)
      .then((res) => {
        toast.success('User created succesfully')
        console.log(res)
        setLoading(false)
      })
      .catch((err) => {
        let errorMessage = err.status === 404 ? 'User or password are not valid.' : 'Something went wrong!'
        toast.error(errorMessage)
        setLoading(false)
      })
  }

  return (
    <LoginBox multiple>
      <ToastContainer />
      <Grid container component="form" onSubmit={handleSubmit(onSubmit)} spacing={3}>
        <LoginHeader>
          <img src={logo} style={{ width: '25vh' }} />
        </LoginHeader>
        <Grid item xs={12} md={6}>
          <TextField
            id="email"
            error={Boolean(errors.email)}
            {...register('email')}
            helperText={errors.email ? errors.email.message : ''}
            disabled={loading}
            name="email"
            label="E-Mail"
            fullWidth
            variant="outlined"
          ></TextField>
        </Grid>
        <Grid item xs={12} md={6}>
          <TextField
            id="name"
            {...register('name')}
            error={Boolean(errors.name)}
            helperText={errors.name ? errors.name.message : ''}
            disabled={loading}
            name="name"
            label="Name"
            fullWidth
            variant="outlined"
          ></TextField>
        </Grid>
        <Grid item xs={12} md={6}>
          <TextField
            id="lastName"
            {...register('lastName')}
            error={Boolean(errors.lastName)}
            helperText={errors.lastName ? errors.lastName.message : ''}
            disabled={loading}
            name="lastName"
            label="Last Name"
            fullWidth
            variant="outlined"
          ></TextField>
        </Grid>
        <Grid item xs={12} md={6}>
          <TextField
            id="gender"
            {...register('gender')}
            error={Boolean(errors.gender)}
            helperText={errors.gender ? errors.gender.message : ''}
            disabled={loading}
            name="gender"
            label="Gender"
            fullWidth
            variant="outlined"
          ></TextField>
        </Grid>
        <Grid item xs={12} md={6}>
          <TextField
            id="password"
            {...register('password')}
            name="password"
            disabled={loading}
            error={Boolean(errors.password)}
            helperText={errors.password ? errors.password.message : ''}
            label="Password"
            autoComplete="on"
            type="password"
            fullWidth
            variant="outlined"
          />
        </Grid>
        <Grid item xs={12} md={6}>
          <TextField
            id="confirmPassword"
            {...register('confirmPassword')}
            name="confirmPassword"
            disabled={loading}
            error={Boolean(errors.confirmPassword)}
            helperText={errors.confirmPassword ? errors.confirmPassword.message : ''}
            label="Confirm Password"
            autoComplete="off"
            type="password"
            fullWidth
            variant="outlined"
          />
        </Grid>
        <LoginButtonContainer>
          <LoadingButton
            disabled={Boolean(isValid) ? false : true}
            loading={loading}
            variant="contained"
            sx={{ width: '50%', marginTop: '1rem' }}
            color="primary"
            type="submit"
          >
            Create Account
          </LoadingButton>
        </LoginButtonContainer>
      </Grid>
    </LoginBox>
  )
}
