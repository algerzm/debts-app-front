import { InputAdornment, TextField } from '@mui/material'
import { styled } from '@mui/material/styles'
import SearchIcon from '@mui/icons-material/Search'
import getLanguage from '../../languages/getLanguageContent'

const SearchStyled = styled(TextField)(({ theme }) => ({
  borderRadius: '4px',
  width: '25vw',
  minWidth: '250px',
  backgroundColor: 'rgba(255, 255, 255, 0.9)',
  '& .MuiOutlinedInput-root': {
    '&.Mui-focused fieldset': {
      borderColor: 'white',
    },
  },
}))

export default function SearchTextField() {
  const { TEXT_SEARCH_FIELD } = getLanguage()
  return (
    <SearchStyled
      InputProps={{
        startAdornment: (
          <InputAdornment position="start">
            <SearchIcon />
          </InputAdornment>
        ),
      }}
      placeholder={TEXT_SEARCH_FIELD}
      color="terciary"
    ></SearchStyled>
  )
}
