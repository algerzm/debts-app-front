import { useState } from 'react'
import { validateEmail, validatePassword, validateFullName } from '../utils/inputValidator'

const useNewAccountForm = () => {
  const [formInputs, setFormInputs] = useState({
    email: '',
    password: '',
    confirmPassword: '',
    name: '',
    lastname: '',
    gender: '',
  })

  const [validationError, setValidationError] = useState({
    email: false,
    password: false,
    name: false,
    lastName: false,
    gender: false,
    confirmPassword: false,
    buttonDisabled: true,
    helperEmailText: '',
    helperPasswordText: '',
    helperConfirmPasswdText: '',
    helperNameText: '',
    helperLastNameText: '',
    helperGenderText: '',
  })

  const { email, password, confirmPassword, name, lastName, gender } = formInputs

  const handleInputChange = (e) => {
    e.preventDefault()
    setFormInputs({
      ...formInputs,
      [e.target.name]: e.target.value,
    })
  }

  const validateInput = (e) => {
    let emailValidState = validationError.email
    let passwdValidState = validationError.password
    let buttonDisabledState = validationError.buttonDisabled
    let confirmPasswordValidState = validationError.confirmPassword
    let nameValidState = validationError.name
    let lastNameValidState = validationError.lastName
    let genderValidState = validationError.gender
    let emailHelperText = ''
    let passwdHelperText = ''
    let passwdConfirmHelperText = ''
    let fullNameHelperText = ''

    if (e.target.name == 'email') {
      validateEmail(email) ? (emailValidState = false) : (emailValidState = true)
    } else if (e.target.name == 'password') {
      validatePassword(password) ? (passwdValidState = false) : (passwdValidState = true)
    } else if (e.target.name == 'confirmPassword') {
      password == confirmPassword ? (confirmPasswordValidState = false) : (confirmPasswordValidState = true)
    } else if (e.target.name == 'name') {
      validateFullName(name) ? (fullNameValidState = false) : (fullNameValidState = true)
    }

    const areInputsEmpty = () => email.trim() != '' && password.trim() != '' && fullName.trim() != '' && confirmPassword.trim() != ''

    const areStatesInFalse = () =>
      emailValidState == false && passwdValidState == false && confirmPasswordValidState == false && fullNameValidState == false

    if (areInputsEmpty() && areStatesInFalse()) {
      buttonDisabledState = false
    } else {
      buttonDisabledState = true
    }

    emailValidState ? (emailHelperText = 'Enter a valid email address.') : (emailHelperText = '')
    passwdValidState ? (passwdHelperText = 'Password must be at least 8 characters.') : (passwdHelperText = '')
    confirmPasswordValidState ? (passwdConfirmHelperText = 'Passwords do not match. Please verify.') : (passwdConfirmHelperText = '')
    fullNameValidState ? (fullNameHelperText = 'Enter a valid name.') : (fullNameHelperText = '')

    setValidationError({
      ...validationError,
      email: emailValidState,
      password: passwdValidState,
      confirmPassword: confirmPasswordValidState,
      fullName: fullNameValidState,
      buttonDisabled: buttonDisabledState,
      helperEmailText: emailHelperText,
      helperPasswordText: passwdHelperText,
      helperConfirmPasswdText: passwdConfirmHelperText,
      helperFullNameText: fullNameHelperText,
    })
  }

  return {
    formInputs,
    validationError,
    handleInputChange,
    validateInput,
  }
}

export default useNewAccountForm
