import { useState, useEffect } from 'react'
import { validateEmail, validatePassword } from '../utils/inputValidator'

const useLoginForm = () => {
  const [formInputs, setFormInputs] = useState({
    email: '',
    password: '',
    confirmPassword: '',
  })

  const [validationError, setValidationError] = useState({
    email: false,
    password: false,
    buttonDisabled: true,
    helperEmailText: '',
    helperPasswordText: '',
  })

  const { email, password } = formInputs

  const handleInputChange = (e) => {
    e.preventDefault()
    setFormInputs({
      ...formInputs,
      [e.target.name]: e.target.value,
    })
  }

  const validateInput = (e) => {
    let emailValidState = validationError.email
    let passwdValidState = validationError.password
    let buttonDisabledState = validationError.buttonDisabled
    let emailHelperText = ''
    let passwdHelperText = ''

    if (e.target.name == 'email') {
      validateEmail(email) ? (emailValidState = false) : (emailValidState = true)
    } else {
      validatePassword(password) ? (passwdValidState = false) : (passwdValidState = true)
    }

    if (email.trim() != '' && password.trim() != '' && emailValidState == false && passwdValidState == false) {
      buttonDisabledState = false
    } else {
      buttonDisabledState = true
    }

    emailValidState ? (emailHelperText = 'Enter a valid email address.') : (emailHelperText = '')
    passwdValidState ? (passwdHelperText = 'Password must be at least 8 characters.') : (passwdHelperText = '')

    setValidationError({
      ...validationError,
      email: emailValidState,
      password: passwdValidState,
      buttonDisabled: buttonDisabledState,
      helperEmailText: emailHelperText,
      helperPasswordText: passwdHelperText,
    })
  }

  return {
    formInputs,
    validationError,
    handleInputChange,
    validateInput,
  }
}

export default useLoginForm
