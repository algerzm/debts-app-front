const es_ES = {
  POSITIVE_BALANCE: 'Saldo a favor',
  NEGATIVE_BALANCE: 'Saldo en contra',
  BALANCE: 'Balance',
  TOTAL_OWED_TOME: 'Deudas a favor',
  TOTAL_OWED_BYME: 'Deudas en contra',
  TOP_DEBTS_TITLE: 'Top Deudas en Contra',
  DASHBOARD_TITLE: 'Panel Principal',
  DASHBOARD_DESCRIPTION: 'Resumen de deudas y balance',
  TEXT_SEARCH_FIELD: 'Buscar',
}

export default es_ES
