const en_US = {
  POSITIVE_BALANCE: 'Total Owed to Me',
  NEGATIVE_BALANCE: 'Total Owed by Me',
  BALANCE: 'Balance',
  TOTAL_OWED_TOME: 'Debts Owed to Me',
  TOTAL_OWED_BYME: 'Debts Owed by Me',
  TOP_DEBTS_TITLE: 'Top Debts I Owe',
  DASHBOARD_TITLE: 'Dashboard',
  DASHBOARD_DESCRIPTION: 'Debts and balance information',
  TEXT_SEARCH_FIELD: 'Search',
}

export default en_US
