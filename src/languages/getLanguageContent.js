import { useSelector } from 'react-redux'
import en_US from './en-US'
import es_ES from './es-ES'

const getLanguage = () => {
  const { language } = useSelector((state) => state.ui)

  switch (language) {
    case 'en-US':
      return en_US
    case 'es-ES':
      return es_ES
    default:
      return en_US
  }
}

export default getLanguage
