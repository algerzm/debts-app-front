import { validate } from 'email-validator'

const validateEmail = (email) => validate(email)

const validatePassword = (password) => {
  if (password.length >= 8) {
    return true
  }

  return false
}

const validateFullName = (fullName) => {
  if (fullName.trim().length > 5) {
    return true
  }

  return false
}

export { validateEmail, validatePassword, validateFullName }
