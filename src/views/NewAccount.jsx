import React from 'react'
import LoginContainer from '../components/login/styled/LoginContainer'
import NewAccountForm from '../components/newAccount/NewAccountForm'

export default function CreateAccount() {
  return (
    <LoginContainer>
      <NewAccountForm></NewAccountForm>
    </LoginContainer>
  )
}
