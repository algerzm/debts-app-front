import React, { useEffect } from 'react'
import { Outlet } from 'react-router-dom'
import { Box, CssBaseline } from '@mui/material'
import { useSelector } from 'react-redux'
import { useNavigate } from 'react-router-dom'

import NavigationDrawer from '../components/mainview/NavigationDrawer'
import OutletBox from '../components/mainview/OutletBox'

export default function MainView() {
  let token = localStorage.getItem('token') || undefined
  let navigate = useNavigate()

  useEffect(() => {
    if (!token) {
      navigate('/')
    }
  }, [])

  const { open } = useSelector((state) => state.drawer)

  return (
    <Box sx={{ display: 'flex', flexDirection: 'row' }}>
      <CssBaseline />
      <NavigationDrawer />
      <OutletBox component="main" open={open} movil={false}>
        <Outlet />
      </OutletBox>
    </Box>
  )
}
