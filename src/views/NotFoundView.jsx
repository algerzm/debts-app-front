import React from 'react'
import oopsImage from '../assets/oops.png'

export default function NotFoundView() {
  return (
    <div
      style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', flexDirection: 'column', height: '100vh', width: '100%' }}
    >
      <img src={oopsImage} style={{ width: '25vw', height: '20vw', marginBottom: '1rem' }} />
      <h1 style={{ color: 'white', fontSize: '2rem', marginTop: '2rem' }}>404 Page Not Found</h1>
    </div>
  )
}
