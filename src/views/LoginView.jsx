import React from 'react'

//Components
import LoginContainer from '../components/login/styled/LoginContainer'
import LoginForm from '../components/login/LoginForm'

export default function LoginView() {
  return (
    <LoginContainer>
      <LoginForm></LoginForm>
    </LoginContainer>
  )
}
