import React, { useEffect, useState } from 'react'
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import { Grid } from '@mui/material'
import { MoneyOff, AttachMoney, AccountBalanceWallet } from '@mui/icons-material'
import getLanguage from '../../languages/getLanguageContent'

import DashboardHeader from '../../components/dashboard/DashboardHeader'
import ResumeCard from '../../components/dashboard/ResumeCard'
import NoDebtsToShow from '../../components/dashboard/NoDebtsToShow'
import DebtsSection from '../../components/dashboard/DebtsSection'
import NewDebtForm from '../../components/dashboard/NewDebtForm'
import OnlineFriendsSection from '../../components/dashboard/OnlineFriendsSection'

import axiosClient from '../../api/axiosClient'

export default function Dashboard() {
  const language = getLanguage()
  const [debts, updateDebts] = useState([])
  let token = localStorage.getItem('token')

  const notify = (message) => toast.success(message)
  const notifyError = (message) => toast.error(message)

  useEffect(async () => {
    if (!token) {
      return null
    }

    await axiosClient
      .get('/debts')
      .then((data) => {
        console.log(data)
        updateDebts(data.data)
      })
      .catch((err) => {
        notifyError(err)
      })
  }, [])

  return (
    <div style={{ width: '100%' }}>
      <ToastContainer theme="light" newestOnTop={true} position="top-right" />
      <DashboardHeader title={language.DASHBOARD_TITLE} description={language.DASHBOARD_DESCRIPTION} />
      <Grid container spacing={3}>
        <Grid item xs={12} sm={4}>
          <ResumeCard colorIcon="green" title={language.POSITIVE_BALANCE} amount={50} TitleIcon={AttachMoney} />
        </Grid>
        <Grid item xs={12} sm={4}>
          <ResumeCard colorIcon="red" title={language.NEGATIVE_BALANCE} amount={570} TitleIcon={MoneyOff} />
        </Grid>
        <Grid item xs={12} sm={4}>
          <ResumeCard colorIcon="black" title={language.BALANCE} amount={-533290} TitleIcon={AccountBalanceWallet} />
        </Grid>
        <Grid item xs={12}>
          <OnlineFriendsSection />
        </Grid>
        <Grid item xs={12}>
          <Grid container spacing={3}>
            <Grid item xs={12} md={8}>
              {debts.length < 1 ? <NoDebtsToShow /> : <DebtsSection debts={debts} title={language.TOP_DEBTS_TITLE} />}
            </Grid>
            <Grid item xs={12} md={4}>
              <NewDebtForm notify={notify} />
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </div>
  )
}
