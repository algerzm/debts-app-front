import React, { useState } from 'react'
import { TextField } from '@mui/material'

export default function tes() {
  const [form, setForm] = useState({
    text1: '',
    text2: '',
  })

  const [lista, setLista] = useState(['Hola', 'Juan Carlos', 'Como estas', 'Chupapija'])

  const handleChange = (e) => {
    setForm({
      ...form,
      [e.target.name]: e.target.value,
    })

    console.log(form)
  }

  const eventClick = () => {
    alert(form.text2)
  }

  const getElementos = () => lista.filter((item) => item !== 'Juan sCarlos')

  return (
    <div style={{ color: 'white' }}>
      <TextField name="text1" color="secondary" onChange={handleChange}></TextField>
      <TextField name="text2" onChange={handleChange}></TextField>
      <button onClick={eventClick}>Hola</button>
      <ul>
        {getElementos().map((item) => (
          <li>{item}</li>
        ))}
      </ul>
    </div>
  )
}
