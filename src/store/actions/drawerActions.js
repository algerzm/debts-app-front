import { TOGGLE_DRAWER, CHANGE_ITEMS_SUCCESS, CHANGE_ITEMS_ERROR, CHANGE_ITEMS } from '../types/drawerTypes'
import axiosClient from '../../api/axiosClient'

/*----------NAVIGATION ITEMS-----------*/
export function changeItemsAction() {
  return async (dispatch) => {
    dispatch(changeItems())

    try {
      //Get drawer items from API
      let items = await axiosClient.get('/drawerItems')

      //Trigger succes action of getting drawer items
      setTimeout(() => {
        dispatch(changeItemsSuccess(items.data))
      }, 1000)
    } catch (e) {
      //Trigger error action of getting drawer items
      dispatch(changeItemsError())
    }
  }
}

const changeItems = () => ({
  type: CHANGE_ITEMS,
})

const changeItemsSuccess = (items) => ({
  type: CHANGE_ITEMS_SUCCESS,
  payload: items,
})

const changeItemsError = () => ({
  type: CHANGE_ITEMS_ERROR,
})
/*----------NAVIGATION ITEMS-----------*/

/*----------TOGGLE DRAWER-------------*/
export function toggleDrawerAction() {
  return (dispatch) => {
    dispatch(toggleDrawer())
  }
}

const toggleDrawer = () => ({
  type: TOGGLE_DRAWER,
})
/*----------TOGGLE DRAWER-------------*/
