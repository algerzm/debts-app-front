import { CHANGE_LANGUAGE } from '../types/uiTypes'

/**--------------LANGUAGE-------------**/
export function changeLanguageAction(language) {
  return (dispatch) => {
    dispatch(changeLanguage(language))
  }
}

const changeLanguage = (language) => ({
  type: CHANGE_LANGUAGE,
  payload: language,
})
/**--------------LANGUAGE-------------**/
