import { combineReducers } from 'redux'
import drawerReducer from './drawerReducer'
import uiReducer from './uiReducer'

export default combineReducers({
  drawer: drawerReducer,
  ui: uiReducer,
})
