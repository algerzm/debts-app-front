import { TOGGLE_DRAWER, CHANGE_ITEMS_SUCCESS, CHANGE_ITEMS, CHANGE_ITEMS_ERROR } from '../types/drawerTypes'

//Each reducer has its own state
const initialState = {
    open: false,
    drawerItems: [],
    loading: false,
    error: false
}

export default function (state = initialState, action) {
    switch (action.type) {
        case CHANGE_ITEMS:
            return {
                ...state,
                loading: true
            }
        case CHANGE_ITEMS_SUCCESS:
            return {
                ...state,
                drawerItems: action.payload,
                loading: false,
                error: false
            }
        case CHANGE_ITEMS_ERROR:
            return {
                ...state,
                loading: false,
                error: true
            }
        case TOGGLE_DRAWER:
            return {
                ...state,
                open: !state.open
            }
        default:
            return state
    }
}