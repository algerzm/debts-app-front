import { CHANGE_LANGUAGE } from '../types/uiTypes'

const initialState = {
  language: 'en-US',
}

export default function (state = initialState, action) {
  switch (action.type) {
    case CHANGE_LANGUAGE:
      return {
        ...state,
        language: action.payload,
      }
    default:
      return state
  }
}
